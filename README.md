javaBasico
==========

Protótipo curso Java Básico.

Projeto sendo desenvolvido para a a disciplina "Projeto integrado de programação e engenharia de software" 
do 5º semestre do curso de Tecnologia em Análise e Desenvolvimento de Sistemas (Pólo BH) da
Universidade Católica de Brasília (Virtual).

Criação de um modelo de treinamento EaD para linguagens de programação, aplicado a uma especificidade do Java 
Básico, que seja atrativo para pessoas interessadas, de fácil utilização e realização e que traga um bom 
embasamento teórico e prático para o aluno.

=> Márcio A. O. Figueiredo
www.bytecno.com.br
adm.bytecno@gmail.com
@BytecnoBH
